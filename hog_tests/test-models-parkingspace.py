from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import cv2
import sys
import imutils

from nms import nms

import numpy as np
from skimage.transform import pyramid_gaussian
from imutils.object_detection import non_max_suppression
from helpers import pyramid, sliding_window

from eval_parkingspace import eval_parkingspace
from datetime import datetime
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

DEBUG = 1


if __name__ == "__main__":
    
    visualize_det = 0
    ifVisualize = 0
    #image_folder = "/Users/agatha/Desktop/eeei_segmented/eeei_5th_solar"
    image_folder = "test_occ_eeei/"
    #image_folder = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/categorize_UFPR04/sorted_UFPR04_images/center-2/"
    image_paths = [f for f in os.listdir(image_folder) if f.endswith('.png')]
    #image_paths = ["20170313-1346_seg12.jpg"]
    #image_paths = ["test_brown.png"]
    #image_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/categorize_UFPR04/sorted_UFPR04_images/left-1/2012-12-12_10_00_05#012.jpg"
    #image_path = "/Users/agatha/Documents/CoE198/parkvsn/ML/image-svm/categorize_UFPR04/sorted_UFPR04_images/left-1/2012-12-12_10_00_05#011.jpg"
    if DEBUG:
        print image_folder
    #fnames = [f for f in os.listdir(".") if f.endswith('.model')]
    fnames = [
        "w30_h80_pos_left-1neg_unocc_left-1_left.model",
        "w30_h80_pos_left-2neg_unocc_left-2_left.model",
        "w40_h80_pos_left-3_neg_unocc_exceptleft.model",
        "w40_h50_pos_center-1neg_unocc_center-1_center.model",
        "w40_h50_pos_center-2neg_unocc_center-2_center.model",
        "w40_h50_pos_center-3neg_unocc_center-3_center.model",
        "w40_h80_pos_right-1_neg_unocc_right-1_except-right.model",
        "w40_h80_pos_right-2neg_unocc_right-2_right.model",
        "w40_h80_pos_right-3neg_unocc_right-3_right.model"
    ]

    if DEBUG:
        print image_paths
    for j, image_path in enumerate(image_paths):
        if DEBUG:
            print image_path
        start_timer = datetime.now()
        num_occupied = 0
        num_models = 0
        for i, model_path in enumerate(fnames):
            
            win_width = int(model_path.split("_")[0].split("w")[1])
            win_height = int(model_path.split("_")[1].split("h")[1])
            if DEBUG:
                print model_path
            actual_path = os.path.join(image_folder, image_path)
            prediction = eval_parkingspace(
                    actual_path, model_path, win_width,
                    win_height, visualize_det, ifVisualize)
            if prediction > 0:
                num_occupied = num_occupied + 1
            if DEBUG:
                print "# Occupied: {}".format(prediction)
                print " "
            num_models = num_models + 1
        #if j == 0:
        end_timer = datetime.now()
        secs = (end_timer - start_timer)
        print "num pred = 1: {}/{}".format(num_occupied, num_models)
        print "time elsapsed: {}".format(secs)
        #break