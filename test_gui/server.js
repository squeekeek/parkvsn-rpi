// server.js

var express = require('express'),
	port = process.env.PORT || 3000,
	app = express(),
	occupancy_array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	stop = false;

app.use(express.static('public'));
app.set('view engine', 'ejs');


// index page 
app.get('/', function(req, res) {
    res.render('pages/index2', {
    	occupancy_array: occupancy_array
    })
});

// about page 
app.get('/about', function(req, res) {
    res.render('pages/about', {
    	occupancy_array: occupancy_array
    })
});

app.get('/stop', (req,res) => {
	console.log("************ STOP *************");
	stop = true;
});

app.get('/send_data', function(req, res) {
	stop = false;
	function receive_occupancy(){
		var exec = require('child_process').exec;
		exec('python dummy_send.py', function(error, stdout, stderr) {
		    console.log('stdout: ' + stdout);
		    
		    //occupancy_array = JSON.parse(stdout); //for laptop testing
		    
		    array_split = stdout.split(':');
		    occupancy_node1 = JSON.parse(array_split[1]);
		    console.log('node 1: ' + occupancy_node1);
		    occupancy_node2 = JSON.parse(array_split[3]);
		    console.log('node 2: ' + occupancy_node2);
		    occupancy_array_str = occupancy_node1 + ',' + occupancy_node2;
		    occupancy_array = occupancy_array_str.split(',');
		    console.log('to send: ' + occupancy_array);
		    for (var x in occupancy_array){
		    	console.log('slot '+x+ ': '+ occupancy_array[x]);
		    }
		    console.log('stderr: ' + stderr);
		    if (error !== null) {
		        console.log('exec error: ' + error);
		    }
		    res.send(occupancy_array);
		});
	}
	receive_occupancy();
});

app.listen(port);
console.log('Started on port: ' + port);