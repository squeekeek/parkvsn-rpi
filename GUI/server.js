// server.js

var express = require('express'),
	port = process.env.PORT || 3000,
	app = express(),
	occupancy_array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	stdout = '',
	stop = false,
	time_interval = 1000;

app.use(express.static('public'));
app.set('view engine', 'ejs');


// index page 
app.get('/', function(req, res) {
    res.render('pages/index2', {
    	time_interval: time_interval
    })
});

// about page 
app.get('/about', function(req, res) {
    res.render('pages/about')
});

app.get('/stop', function(req,res) {
	console.log("************ STOP *************");
	stop = true;
});

app.get('/init_controller', function(req, res) {
	function init_controller(){
		var exec = require('child_process').exec;
		exec('sudo python scripts/init_controller.py', function(error, stdout, stderr) {
		    console.log('stdout: ' + stdout);
		    console.log('stderr: ' + stderr);
		    if (error !== null) {
		        console.log('exec error: ' + error);
		    }
		    res.send(stdout);
		});
	}
	init_controller();
});

app.get('/measure', function(req, res) {
	function measure(){
		var exec = require('child_process').exec;
		exec('sudo python scripts/measure_metrics.py', function(error, stdout, stderr) {
		    console.log('stdout: ' + stdout);
		    console.log('stderr: ' + stderr);
		    if (error !== null) {
		        console.log('exec error: ' + error);
		    }
		});
		res.redirect('back');
	}
	measure();
});

app.get('/update_slots', function(req, res) {
	function update_slots(){
		var exec = require('child_process').exec;
		exec('sudo python scripts/update_slots.py', function(error, stdout, stderr) {
		    console.log('stdout: ' + stdout);
		    console.log('stderr: ' + stderr);
		    if (error !== null) {
		        console.log('exec error: ' + error);
		    }
		});
		res.end();
	}
	update_slots();
});

app.get('/init_slots1', function(req, res) {
	function init_slots1(){
		var exec = require('child_process').exec;
		exec('sudo python scripts/init_slots1.py', function(error, stdout, stderr) {
		    console.log('stdout: ' + stdout);
		    console.log('stderr: ' + stderr);
		    if (error !== null) {
		        console.log('exec error: ' + error);
		    }
		    res.render('pages/label_spaces1');
		});
	}
	init_slots1();
});

app.get('/init_slots2', function(req, res) {
	function init_slots2(){
		var exec = require('child_process').exec;
		exec('sudo python scripts/init_slots2.py', function(error, stdout, stderr) {
		    console.log('stdout: ' + stdout);
		    console.log('stderr: ' + stderr);
		    if (error !== null) {
		        console.log('exec error: ' + error);
		    }
		});
	}
	init_slots2();
	res.render('pages/label_spaces2');
});

var io = require('socket.io-client'),
    //socket = io.connect('http://localhost:4000');
socket = io.connect('https://parakeet.sandnox.com');

var msg = "4thsolar";
var empty = 0;
var total = 9;
var slots = [0, 0, 0, 0, 0, 0, 0, 0, 0];
socket.on('connect', function() {
    console.log("socket connected");
    //var value = "Hello";
    //socket.emit('messageChange', { message: value });

    setInterval(function() {
            socket.emit('rpi_msg', {
                message: msg,
                empty: empty,
                total: total,
                slots: slots,
            })
        },
        40000);
});

app.get('/send_data', function(req, res) {
	stop = false;
	function receive_occupancy(){
		var exec = require('child_process').exec;
		exec('sudo python scripts/send_occupancy_array.py', function(error, stdout, stderr) {
		    console.log('stdout: ' + stdout);
		    
		    //occupancy_array = JSON.parse(stdout); //for laptop testing
		    
		    array_split = stdout.split(':');
		    occupancy_node1 = JSON.parse(array_split[1]);
		    slots = occupancy_node1;
		    total = slots.length;
		    empty = 0;
		    for(var i = 0; i < slots.length; i++){
		    	if(slots[i] == 0){
		    		empty++;
		    	}
		    }
		    console.log('node 1: ' + occupancy_node1);
		    occupancy_node2 = JSON.parse(array_split[3]);
		    console.log('node 2: ' + occupancy_node2);
		    occupancy_array_str = occupancy_node1 + ',' + occupancy_node2;
		    occupancy_array = occupancy_array_str.split(',').map(Number);
		    console.log('to send: ' + occupancy_array);
		    
		    console.log('stderr: ' + stderr);
		    if (error !== null) {
		        console.log('exec error: ' + error);
		    }
		    res.send(occupancy_array);
		});
	}
	receive_occupancy();
});

app.listen(port);
console.log('Started on port: ' + port);