from __future__ import division
import cv2
import os
import xmltodict as xmld

import img_math
import img_predict
import img_slice
import img_rectify


def get_occupancy_list(img, spaces_dict):
    ref_img_path = 'parakeet_bg_ref.jpg'

    bsobj = img_predict.ParakeetBackgroundSubtractor()

    if os.path.isfile(ref_img_path):
        img_ref = cv2.imread(ref_img_path, 0)
        bsobj.set_ref_img(img_ref)

    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_masked = bsobj.feed(img_gray)
    img_masked_imp = cv2.dilate(img_masked, cv2.getStructuringElement(cv2.MORPH_RECT, (10, 10)))
    nlabels, labels, stat, centroids = cv2.connectedComponentsWithStats(img_masked_imp)

    if 'calibrate' in spaces_dict:
        img_sliced, prect = img_slice.slice_img(img_masked_imp, spaces_dict)
    else:
        rect_trans, chulls, _, (pspaces_bottom, pspaces_top) = img_rectify.rectify(img, spaces_dict, spaces_dict['calibrate']['vert_pts'], spaces_dict['calibrate']['horiz_pts'], spaces_dict['calibrate']['line_vec'], slice_spaces=True)

    for rectidx, (isfull, eachrect) in enumerate(prect):
        prect[rectidx][0] = 0

        for k, eachcenter in enumerate(centroids):
            if img_math.is_point_in_rect(eachcenter, eachrect):
                if stat[k, cv2.CC_STAT_AREA] >= 0.2 * img_math.polygon_area(eachrect):
                    prect[rectidx][0] = 1
                    break

    occlist, _ = zip(*prect)
    return zip(range(0, len(occlist)), occlist)


def main():
    test_path = '../datasets/EEEI/eeei_5th_solar_20170310/20170311-1626'
    img = cv2.imread(test_path + '.jpg', 1)

    with open(test_path + '.xml') as fh:
        pspaces_dict = xmld.parse(fh.read())

        print get_occupancy_list(img, pspaces_dict)

if __name__ == "__main__":
    main()