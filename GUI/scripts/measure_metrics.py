#!/usr/bin/env python

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24 as nrf

from base64 import b64decode, b64encode
import zlib
import time

import cv2
import numpy as np

role = "controller"
inp_role = '0'


nrf.config()
nrf.controller_config()

#node 1
nrf.config_to_node1()
nrf.sendPacket("SEND METRICS 1")

ack_ping = nrf.recvPacket(1)
print('Ack : {}'.format(ack_ping))
if(ack_ping == "NODE 1"):

	ping_array = []
	throughput_array = []
	accuracy_array = []
	counter = 0

	print(' ************ NETWORK METRICS 1 *********** ')
	
	while(counter<10):
		time_begin = time.time()
		nrf.sendPacket("PING")
		received = nrf.recvPacket(1)
		ping_time = time.time() - time_begin
		#new_log.write(str(ping_time))
		print('Ping: {}'.format(ping_time))
		counter += 1
	'''

	nrf.sendPacket("START")
	time_begin = time.time()
	received = nrf.recvPic(1)
	throughput_time = time.time() - time_begin
	throughput = float(len(received)/throughput_time)
	print('Throughput: {} bytes/second'.format(throughput))
	'''
'''
nrf.config_to_node2()
nrf.sendPacket("SEND METRICS 2")

ack_ping = nrf.recvPacket(1)
print('Ack : {}'.format(ack_ping))
if(ack_ping == "NODE 2"):

	ping_array = []
	throughput_array = []
	accuracy_array = []
	counter = 0

	print(' ************ NETWORK METRICS 2 *********** ')

	time_begin = time.time()
	nrf.sendPacket("PING")
	received = nrf.recvPacket(1)
	ping_time = time.time() - time_begin
	print('Ping: {} seconds'.format(ping_time))

	nrf.sendPacket("START")
	time_begin = time.time()
	received = nrf.recvPic(1)
	throughput_time = time.time() - time_begin
	throughput = float(len(received)/throughput_time)
	throughput_array.append(throughput)
	print('Throughput: {} bytes/second'.format(throughput))
'''
	