#!/usr/bin/env python

#
# PyNRF24 Boilerplate
#
from __future__ import print_function
import time
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import hashlib

import logging
import sys

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s (%(threadName)-2s) %(message)s')


irq_gpio_pin = None

interrupt_stop = False

# The write addresses of the peripheral nodes
addr_central_rd = [0xF0F0F0F0C1, 0xF0F0F0F0D1, 0xF0F0F0F0E1, 0xF0F0F0F0F1, 0xF0F0F0F0A1]

# The read addresses of the peripheral nodes
addr_central_wr = [0xF0F0F0F0AB, 0xF0F0F0F0BC, 0xF0F0F0F0DE, 0xF0F0F0F0FA, 0xF0F0F0F0BA]

inp_role = 'none'

#Timeout in seconds for the controller to receive msg from node
timeout = 5

# to accomodate for the 5 reading pipes/nodes, not including the first one
found_nodes = [0, 0, 0 ,0 ,0 ,0]

role = 'none'

radio = RF24(17, 0)

radio.begin()
radio.openReadingPipe(1, addr_central_rd[0])
radio.openReadingPipe(2, addr_central_rd[1])
time.sleep(1)
radio.openWritingPipe(addr_central_wr[0])
radio.stopListening()
radio.flush_tx()
time.sleep(1)
data_to_send = "Found Node 1"
if(radio.write(data_to_send)):
    print('Written to Node 1')