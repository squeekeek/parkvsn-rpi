#!/usr/bin/env python

'''

For Image Processing and HOG-SVM, insert your code starting at line 163

'''

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24_temp as nrf
import random

import cv2
import numpy as np

import logging

import signal
import sys

radio = RF24(17, 0)
ack_stop = "STOP"
inp_mode = 'none'
mode = -1

def signal_term_handler(signal, frame):
	if (nrf.found_nodes[0]):               
		radio.openWritingPipe(nrf.addr_central_wr[0])
		counter = 0
		while(counter < 10):
			radio.writeAckPayload(1,'INTERRUPT_STOP')
			counter += 1
			time.sleep(0.1)

	if (nrf.found_nodes[1]):
		radio.openWritingPipe(nrf.addr_central_wr[1])
		while(counter < 10):
			radio.writeAckPayload(2,"INTERRUPT_STOP")
			counter += 1
			time.sleep(0.1)
	
	mode = nrf.chooseMode()
	runMode(mode)

signal.signal(signal.SIGINT, signal_term_handler)

def runMode(mode):
	if (mode == 0):
		measureMetrics()
	elif (mode == 1):
		sendOccupiedArray()
	elif (mode == 2):
		sendFeatures()
	elif (mode == 3):
		sys.exit(0)

def measureMetrics():
	global role
	image = cv2.imread("Lenna.png")
	win_size = (64, 128)
	image = cv2.resize(image, win_size)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	hog = cv2.HOGDescriptor()
	h = hog.compute(gray)
	num = 0

	if(role == "controller"):
		ping_array = []
		throughput_array = []
		accuracy_array = []

		print(' ************ NETWORK METRICS *********** ')
		print('1. Measuring ping')
		time_begin = time.time()
		data_to_send = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEF" #32 bytes
		counter = 0
		if(radio.write(data_to_send)):
			if(radio.isAckPayloadAvailable()):
				length = radio.getDynamicPayloadSize()
				received = radio.read(length)
				time_elapsed = time.time() - time_begin
				ping_array.append(time_elapsed*1000)
				print('    PING: {} ms'.format(time_elapsed*1000))

		else:
			print('Failed to ping node')
			mode = nrf.chooseMode()
			runMode(mode)

		print('2. Measuring throughput')
		data_to_send = json.dumps(h.tolist())
		data_size = len(data_to_send.encode('utf-8'))
		time_begin = time.time()
		if(not nrf.sendString(data_to_send)):
			mode = nrf.chooseMode()
			runMode(mode)
		time_elapsed = time.time() - time_begin
		throughput = float((data_size*8)/time_elapsed)/1000000
		throughput_array.append(throughput)
		print('    TIME: {} seconds'.format(time_elapsed))
		print('    SIZE: {} bytes'.format(data_size))
		print('    THROUGHPUT: {} Mbps'.format(throughput))
		
		print('3. Measuring accuracy (node operation)')
		received = nrf.recvString()
		print('{}'.format(received))

		mode = nrf.chooseMode()
		runMode(mode)

	if(role == "node"):
		while True:
			if(radio.available()):
				length = radio.getDynamicPayloadSize()
				received = radio.read(length)
				radio.writeAckPayload(1, "PING")
				print('received ping')
				break
		
		#Receive features to measure throughput
		received = nrf.recvString()
		print('received string')

		#format received string to array
		received_temp = np.array(json.loads(received))
		received_array = received_temp.flatten()
		#format in-node processed features to array
		temp = json.dumps(h.tolist())
		data_to_compare = np.array(json.loads(temp))
		stored_array = data_to_compare.flatten()
		#compare received array to stored array
		counter = 0
		error = 0
		for element in stored_array:
			if(not stored_array[counter]==received_array[counter]):
				error += 1
		data_to_send = "    ACCURACY: "+str(float((len(stored_array)-error)/len(stored_array))*100)+"%"
		nrf.sendString(data_to_send)

		mode = nrf.chooseMode()
		runMode(mode)

def sendStringLoop():
	global role
	while True:
		if (role == "node"):
			data_to_send = "NINETY MILES OUTSIDE CHICAGO CANT STOP DRIVING I DONT KNOW WHY SO MANY QUESTIONS I NEED AN ANSWER TWO YEARS LATER YOU STILL ON MY MIIIIIND"
			if(not nrf.sendString(data_to_send)):
				print('entered if')
				mode = nrf.chooseMode()
				runMode(mode)
			time.sleep(1)
		if (role == "controller"):
			received = nrf.recvString()
			print('Received string: {}'.format(received))

def backgroundThreshold():
	occupancyArray = []

	#insert code here

	return occupancyArray

def hogSVM():
	occupancyArray = []

	#insert code here

	return occupancyArray

def sendOccupiedArray():
	global role, inp_role
	# ping to node 1
	counter = 0
	while True:
		if(role == "controller"):
			if (counter%2 == 1 and nrf.found_nodes[0]):
				
				radio.openWritingPipe(nrf.addr_central_wr[0])

				nrf.sendPacket("PING NODE 1")
				ack_ping = nrf.recvPacket(1)
				if(ack_ping == "NODE 1"):
					print('*************************************')
					print('Now accepting data from Node 1')
					received = nrf.recvTCP(1)
					counter2 = 0
					while(not received == "DONE SENDING ARRAY"):
						print('[{}] Occupancy Array: {}'.format(counter2, received))
						received = nrf.recvTCP(1)
						counter2 += 1

			# ping to node 2
			if (counter%2 == 0 and nrf.found_nodes[1]):
				radio.openWritingPipe(nrf.addr_central_wr[1])

				nrf.sendPacket("PING NODE 2")
				ack_ping = nrf.recvPacket(2)
				if(ack_ping == "NODE 2"):
					print('*************************************')
					print('Now accepting data from Node 2')
					received = nrf.recvTCP(2)
					counter2 = 0
					while(not received == "DONE SENDING ARRAY"):
						print('[{}] Occupancy Array: {}'.format(counter2, received))
						received = nrf.recvTCP(2)
						counter2 += 1

			counter += 1

		if(role == "node"):
			received = nrf.recvPacket(1)
			while(not received == ("PING NODE "+inp_role)):
				received = nrf.recvPacket(1)
			print('PING RECEIVED: {}'.format(received))
			nrf.sendPacket("NODE "+inp_role)

			#BACKGROUND THRESHOLDING
			BT_result = backgroundThreshold()
			#send result as string
			for digit in BT_result:
				BT_string += (str(digit)+',')
			BT_string = inp_role+"-DATA-BT: ["+BT_string[:-1]+"]\n"

			#HOG+SVM
			HOG_result = hogSVM()
			for digit in HOG_result:
				HOG_array += (str(digit)+',')
			HOG_array = inp_role+"-DATA-HOG: ["+HOG_array[:-1]+"]"

			data_to_send = BT_result + HOG_array
			#occupancy_array = [1,0,1,0,0,0,1,0,1,0,0,0,1,1,1,1,1,1,1,1,1,1]
			#data_to_send = str(occupancy_array)
			for i in range(10):
				if(not nrf.sendTCP(data_to_send)):
					mode = nrf.chooseMode()
					runMode(mode)
			nrf.sendTCP("DONE SENDING ARRAY")


def sendFeatures():
	global role
	image = cv2.imread("Lenna.png")
	win_size = (64, 128)
	image = cv2.resize(image, win_size)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	hog = cv2.HOGDescriptor()
	h = hog.compute(gray)

	while True:
		if (role == "node"):
			data_to_send = json.dumps(h.tolist())
			print('feature: {}'.format(data_to_send))
			if(not nrf.sendString(data_to_send)):
				mode = nrf.chooseMode()
				runMode(mode)
			time.sleep(1)
		if(role == "controller"):
			received = nrf.recvString()
			print('received feature: {}'.format(received))	
			temp = np.array(json.loads(received))
			received_array = temp.flatten()
			for x in received_array:
				print('array element: {}'.format(x))

nrf.config()
inp_role, role = nrf.userDefineRoles()
print('role: {}'.format(role))

if (role == "controller"):
	nrf.findNodes()
if (role == "node"):
	nrf.waitStartNormal()

mode = nrf.chooseMode()
#print('mode: {}'.format(mode))
runMode(mode)
