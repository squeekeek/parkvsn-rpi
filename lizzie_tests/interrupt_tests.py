import signal
import sys
import time

def signal_term_handler(signal, frame):
	print "caught signal"
	initialize()
	
def initialize():
	inp = str(input('1 for printing 2 for exit '))
	if(inp == '2'):
		sys.exit(0)

signal.signal(signal.SIGINT, signal_term_handler)

while True:
	print "blah"
	time.sleep(1)
