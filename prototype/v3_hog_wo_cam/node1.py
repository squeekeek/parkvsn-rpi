#!/usr/bin/env python

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24 as nrf
import random

import cv2
import numpy as np
import picamera
from picamera.array import PiRGBArray
from base64 import b64decode, b64encode
import zlib
from PIL import Image

import os
import glob
import time
from _prototype_v3_wo_cam.sendOccupancyLists import sendOccupancyLists

role = "node"
inp_role = '1'

def waitCommand():
    while True:
        received = nrf.recvPacket(1)
        print('Received command: {}'.format(received))
        return received

'''
sends the occupancy array in string format
to do: insert HOG and BT arrays
'''
def sendOccupancyArray():
    nrf.sendPacket("NODE "+inp_role)

    # Load spaces dict
    spaces_path = os.path.join("_prototype_v3_wo_cam", "config", "markings.json")
    with open(spaces_path) as data_file:
        spaces_dict = json.load(data_file)


    # Load hog setting
    hog_dict_path = os.path.join("_prototype_v3_wo_cam","config", "hog.json")
    with open(hog_dict_path) as data_file:
        hog_dict = json.load(data_file)

    hog_setting = None
    hog_option = "blk-2_cell-6"
    for option in hog_dict["hog"]:
        if hog_option == option["id"]:
            hog_setting = option
            break

    # Load detection dict
    detection_dict_path = os.path.join("_prototype_v3_wo_cam","config", "detection.json")
    with open(detection_dict_path) as data_file:
        detection_dict = json.load(data_file)

    # Load model folder and dict
    hog_models_folder = os.path.join("_prototype_v3_wo_cam","config", "models_hog")
    models_dict_path = os.path.join(hog_models_folder, "models.json")
    with open(models_dict_path) as data_file:
        hog_models_dict = json.load(data_file)



    # Load image (replace with cam version)
    sample_image_path = os.path.join("_prototype_v3_wo_cam","sample_images", "*")
    for im_path in glob.glob(sample_image_path):
        occupancyList_BGS, occupancyList_HOG, occupancyList_final, time_evaluated = sendOccupancyLists(
            im_path,
            spaces_dict,
            hog_setting,
            detection_dict,
            hog_models_folder,
            hog_models_dict
        )
    #occupancy_array = [1,0,1,0,0,0,1,0,1,0,0,0,1,1,1,1,1,1,1,1,1,1]
    #HOG_array = [0,0,1,0,1,0,1,0,1,0,0,0,1,1,1,0,1,1,1,1,1,1]
    #BT_array = [1,0,1,0,0,0,1,0,1,0,0,0,1,1,1,1,0,1,1,1,1,1]

	print("Time evaluated: {}".format(time_evaluated))
    data_to_send = str(occupancyList_final)
    if(not nrf.sendTCP(data_to_send)):
        mode = nrf.chooseMode()
        runMode(mode)

def initSlots():
    nrf.sendPacket("NODE "+inp_role)

    #initialize camera
    camera = picamera.PiCamera()
    rawCapture = PiRGBArray(camera)
    time.sleep(0.1)

    #take picture
    camera.capture(rawCapture, format="bgr")
    image = rawCapture.array
    cv2.imwrite('test_image.jpg', image)
    
    #can move to exit function later
    camera.close()

    foo = Image.open('test_image.jpg')
    foo.save("compressed_opt.jpg",optimize=True,quality=75)

    with open("compressed_opt.jpg", "rb") as f:
        data = f.read()
        encoded_data = data.encode("base64")

    print('{}'.format(encoded_data))
    nrf.sendTCP(encoded_data)

if __name__ == "__main__":
    nrf.config()
    nrf.node1_config()
    nrf.waitStartNormal()

    while True:
        cmd = waitCommand()
        if(cmd == ('SEND OCCUPANCY ARRAY '+inp_role)):
            print('Waiting for Occupancy Array')
            sendOccupancyArray()
            cmd = 'WAITING'
        if(cmd == ('INITIALIZE SLOTS '+inp_role)):
            print('Waiting for Parking Lot picture')
            initSlots()
            cmd = 'WAITING'