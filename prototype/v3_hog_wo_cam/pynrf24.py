#!/usr/bin/env python

#
# PyNRF24 Boilerplate
#
from __future__ import print_function
import time
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import hashlib

import logging
import sys

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s (%(threadName)-2s) %(message)s')


irq_gpio_pin = None

interrupt_stop = False

# The write addresses of the peripheral nodes
addr_central_rd = [0xF0F0F0F0C1, 0xF0F0F0F0D1, 0xF0F0F0F0E1, 0xF0F0F0F0F1, 0xF0F0F0F0A1]

# The read addresses of the peripheral nodes
addr_central_wr = [0xF0F0F0F0AB, 0xF0F0F0F0BC, 0xF0F0F0F0DE, 0xF0F0F0F0FA, 0xF0F0F0F0BA]

inp_role = 'none'

#Timeout in seconds for the controller to receive msg from node
timeout = 5

# to accomodate for the 5 reading pipes/nodes, not including the first one
found_nodes = [0, 0, 0 ,0 ,0 ,0]

role = 'none'

radio = RF24(17, 0)

millis = lambda: int(round(time.time() * 1000))

def chooseMode():

    print('inside choose mode')
    global role, inp_mode, inp_role
    inp_mode = 'none'
    if(role == "controller"):
        if(role == "controller"):
            print(' ************ Choose Mode *********** ')
        while (inp_mode !='0') and (inp_mode !='1') and (inp_mode !='2') and (inp_mode != '3'):
            inp_mode = str(input('Choose a mode: \n 0: Measure metrics \n 1: Send occupied array \n 2: Send features \n 3: exit \n'))
        radio.stopListening()

        if (found_nodes[0]):               
            radio.openWritingPipe(addr_central_wr[0])
            sendPacket(inp_mode)

        # node 2
        if (found_nodes[1]):
            radio.openWritingPipe(addr_central_wr[1])
            sendPacket(inp_mode)
        
        #print('outside loop')
        if (inp_mode == '0'):
            return 0
        elif (inp_mode == '1'):
            return 1
        elif (inp_mode == '2'):
            return 2
        elif (inp_mode == '3'):
            #print('inside')
            sys.exit(0)

    if(role == "node"):
        radio.startListening()
        received = recvPacket(1)
        if(received.decode('utf-8')=='0'):
            print('received 0')
            return 0
        elif(received.decode('utf-8')=='1'):
            print('received 1')
            return 1
        elif(received.decode('utf-8')=='2'):
            print('received 2')
            return 2
        elif(received.decode('utf-8')=='3'):
            sys.exit(0)

def sendPacket(send_packet):
    global radio
    #print('start sending packet')
    radio.stopListening()
    radio.flush_tx()
    while True:
        if(radio.write(send_packet)):
            timeout = False
            time_start = millis()
            while(not radio.isAckPayloadAvailable() and (not timeout)):
                if(millis()-time_start > 500):
                    timeout = True
            if(not timeout):
                length = radio.getDynamicPayloadSize()
                received = radio.read(length)
                #print('received ack payload: {}'.format(received.decode('utf-8')))
                if(send_packet == received.decode('utf-8')):
                    radio.write("ACK")
                    return True
                else:
                    if(received.decode('utf-8')=="INTERRUPT_STOP"):
                        return False
                    else:
                        radio.write("SEND AGAIN")
        time.sleep(0.0001)

def recvPacket(pipeNo):
    global radio, inp_role

    radio.startListening()
    #print('start listening for packet')
    while True:
        if(radio.available()):
            length = radio.getDynamicPayloadSize()
            received = radio.read(length)
            radio.writeAckPayload(pipeNo, received)
            #print('received packet: {}'.format(received.decode('utf-8')))
            timeout = False
            time_start = millis()
            while(not radio.available() and (not timeout) and (not received.decode('utf-8')=="ACK") and (not received.decode('utf-8')=="SEND AGAIN")):
                if(millis()-time_start > 1000):
                    #print('timeout')
                    timeout = True
            if(not timeout):
                length = radio.getDynamicPayloadSize()
                received_ack = radio.read(length)
                #print('response packet: {}'.format(received_ack.decode('utf-8')))
                if(received_ack.decode('utf-8')=="ACK"):
                    return received.decode('utf-8')
            else:
                pass
        else:
            pass

def sendTCP(data_to_send):
    packet_list = parsePacket(data_to_send)
    for num in range(len(packet_list)):
        print('NOW SENDING: {}'.format(packet_list[num]))
        if(not sendPacket(packet_list[num])):
            return False
    if(not sendPacket("DONE SENDING STRING")):
        return False
    else:
        return True

def recvTCP(pipeNo):
    recv_buffer = []

    while True:
        received = recvPacket(pipeNo)
        if(received=="DONE SENDING STRING"):
            whole_string = ''.join(recv_buffer)
            break
        else:
            recv_buffer.append(received)
            print('RECEIVED: {}'.format(received))

    return whole_string

def parsePacket(data_to_send):
    """
        Accepts string
        Returns list of 32 byte strings (if applicable)
        For sending packets
    """
    packet_list = []
    len_data = len(data_to_send)

    i = 0
    while (1):
        starting = i*32
        if (len_data <= 32):
            end = starting + len_data
            packet_list.append(data_to_send[starting:end])
            break
        else:
            end = starting + 32
            packet_list.append(data_to_send[starting:end])
            len_data = len_data - 32
        i = i + 1

    return packet_list


def config():
    global radio
    # Radio Number: GPIO number, SPI bus 0 or 1
    radio.begin()
    radio.enableAckPayload()
    radio.setAutoAck(True)
    radio.enableDynamicPayloads()
    radio.setRetries(5,15)
    radio.setPALevel(RF24_PA_MIN) #if close together

def node1_config():
    global radio  
    print('Role: node1 to be accessed, awaiting transmission')
    radio.openWritingPipe(addr_central_rd[0])
    radio.openReadingPipe(1, addr_central_wr[0])
    time.sleep(1)

def node2_config():
    global radio
    print('Role: node2 to be accessed, awaiting transmission')
    radio.openWritingPipe(addr_central_rd[1])
    radio.openReadingPipe(1,addr_central_wr[1])
    time.sleep(1)

def config_to_node1():
    radio.openWritingPipe(addr_central_wr[0])

def config_to_node2():
    radio.openWritingPipe(addr_central_wr[1])

def controller_config():
    print('Role: Controller, starting transmission')
    radio.openReadingPipe(1, addr_central_rd[0])
    radio.openReadingPipe(2, addr_central_rd[1])
    time.sleep(1)
    

def userDefineRoles():
    """
        Description:
            CLI based user input roles
            Can be extended to 5 peripheral nodes by copy-pasting
        Return:
            Tuple: (inp_role, role)
    """
    global inp_role, radio, camera, rawCapture, role

    print(' ************ Role Setup *********** ')
    while (inp_role !='1') and (inp_role !='2'):
        inp_role = str(input('Choose a role: 1 for node1, 2 for node2 CTRL+C to exit) '))


    if (inp_role == '0'):
        print('Role: Controller, starting transmission')
        radio.openReadingPipe(1, addr_central_rd[0])
        radio.openReadingPipe(2, addr_central_rd[1])
        time.sleep(1)
        # TODO: can insert up to 5 readng pipes
        role = "controller"
        counter = 0

    elif (inp_role == '1'):
        # HAS OPENCV
        print('Role: node1 to be accessed, awaiting transmission')
        radio.openWritingPipe(addr_central_rd[0])
        radio.openReadingPipe(1, addr_central_wr[0])
        time.sleep(1)
        role = "node"
        counter = 0

    elif (inp_role == '2'):
        print('Role: node2 to be accessed, awaiting transmission')
        radio.openWritingPipe(addr_central_rd[1])
        radio.openReadingPipe(1,addr_central_wr[1])
        time.sleep(1)
        role = "node"
        counter = 0

    radio.startListening()
    return (inp_role, role)

def findNodes():

    global radio, addr_central_wr
    counter = 0

    #Controller tests node 1
    radio.openWritingPipe(addr_central_wr[0])
    radio.stopListening()
    time.sleep(1)

    #write to node 1
    data_to_send = "Found Node 1"
    while(counter < 20):
        if(radio.write(data_to_send)):
            print('Written to Node 1')
            found_nodes[0]=1
            break
        else:
            counter += 1
        time.sleep(0.1)
    if(counter == 4):
        print('Did not find Node 1')
        radio.closeReadingPipe(1)

    radio.openWritingPipe(addr_central_wr[1])
    time.sleep(1)
    counter = 0

    #write to node 2
    data_to_send = "Found Node 2"
    while(counter < 20):
        if(radio.write(data_to_send)):
            print('Written to Node 2')
            found_nodes[1]=1
            break
        else:
            counter += 1
        time.sleep(0.1)
    if(counter == 4):
        print('Did not find Node 2')
        radio.closeReadingPipe(2)

    for num in range(len(found_nodes)):
        if(found_nodes[num]==1):
            radio.openWritingPipe(addr_central_wr[num])

def waitStartNormal():

    global radio

    radio.startListening()

    while True:
        if(radio.available()):
            length = radio.getDynamicPayloadSize()
            received = radio.read(length)
            print('Received: {}'.format(received.decode('utf-8')))
            break

def restartRadio():
    radio.failureDetected = 0
    radio.begin()

    if (inp_role == '0'):
        print('Role: Controller, starting transmission')
        radio.openReadingPipe(1, addr_central_rd[0])
        radio.openReadingPipe(2, addr_central_rd[1])
        radio.openWritingPipe(addr_central_wr[0])
        radio.openWritingPipe(addr_central_wr[1])
        time.sleep(1)
        radio.stopListening()

    elif (inp_role == '1'):
        # HAS OPENCV
        print('Role: node1 to be accessed, awaiting transmission')
        radio.openWritingPipe(addr_central_rd[0])
        radio.openReadingPipe(1, addr_central_wr[0])
        time.sleep(1)
        radio.startListening()

    elif (inp_role == '2'):
        print('Role: node2 to be accessed, awaiting transmission')
        radio.openWritingPipe(addr_central_rd[1])
        radio.openReadingPipe(1,addr_central_wr[1])
        time.sleep(1)
        radio.startListening()