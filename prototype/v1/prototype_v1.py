#!/usr/bin/env python

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24_temp as nrf

import cv2
import numpy as np

import logging

import signal
import sys

from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils

from p1_hog_svm import hog_svm

radio = RF24(17, 0)
ack_stop = "STOP"
inp_mode = 'none'
mode = -1

def signal_term_handler(signal, frame):
    counter = 0
    while(counter < 5):
        if(radio.writeAckPayload(1, ack_stop)):
            break
        time.sleep(0.5)
        counter += 1
    print('wrote ack s')
    mode = nrf.chooseMode()
    runMode(mode)

signal.signal(signal.SIGINT, signal_term_handler)


def sendOccupiedArray():
    global role, inp_role
    while True:
        if (role == "node"):
            data_to_send = ''

            # STATE 1: SEGMENT IMAGES INTO PARKING LOT SPACES
            # INSERT CODE FOR SEGMENTING IMAGES
            # TEMP CODE
            image_folder = "eeei_5th_solar"
            image_paths = [f for f in os.listdir(image_folder) if f.endswith('.jpg')]

            # STATE 2: BACKGROUND THRESHOLDING
            # TEMP CODE
            BT_result = []
            for i in range(0, len(image_paths), 1):
                BT_result.append(0)
            # send result
            for digit in BT_result:
                data_to_send += (str(digit)+',')
            data_to_send = inp_role+"-DATA-BT: ["+data_to_send[:-1]+"]"
            if(not nrf.sendString(data_to_send)):
                mode = nrf.chooseMode()
                runMode(mode)

            #STATE2: HOG + SVM

            # Load json for models to use 
            json_path = "p1_hog_svm_models.json"
            with open(json_path) as data_file:
                json_dict = json.load(data_file)

            HOG_result = []

            for i, image_path in enumerate(image_paths):
                actual_path = os.path.join(image_folder, image_path)
                image = cv2.imread(actual_path)
                predict = hog_svm(image, json_dict)
                HOG_result.append(predict)

            for digit in HOG_result:
                data_to_send += (str(digit)+',')
            data_to_send = inp_role+"-DATA-HOG: ["+data_to_send[:-1]+"]"
            if(not nrf.sendString(data_to_send)):
                mode = nrf.chooseMode()
                runMode(mode)

            mode = nrf.chooseMode()
            runMode(mode)

        if (role == "controller"):
            print('********** REQUESTED DATA *********')
            #RECEIVE BACKGROUND THRESHOLDING
            received = nrf.recvString()
            if(received[0:8]=="1-DATA-BT"):
                array_result = received[11:]
                bt_array_temp = array_result[12:-1].split(',')
                for digit in bt_array_temp:
                    bt_array.append(digit)
            print('Background Thresholding: {}'.format(array_result))

            #RECEIVE HOG+SVM
            received = nrf.recvString()
            if(received[0:9]=="1-DATA-HOG"):
                array_result = received[12:]
                hog_array_temp = array_result[12:-1].split(',')
                for digit in hog_array_temp:
                    hog_array.append(digit)
            print('HOG+SVM: {}'.format(array_result))

            #parse data
            counter = 0
            obstructionArray = []

            for num in bt_array:
                if(bt_array[counter] and hog_array[counter]):
                    occupancyArray.append(1)
                    obstructionArray.append(0)
                elif bt_array[counter] == 1:
                    obstructionArray.append(1)
                else:
                    occupancyArray.append(0)
                    obstructionArray.append(0)
                counter += 1
            print('Occupancy Array: {}'.format(occupancyArray))
            print('Obstruction Array: {}'.format(obstructionArray))
            print('***********************************')

def segmentParkingSpaces():
    #insert code here
    return True

def backgroundThreshold():
    occupancyArray = []
    #insert code here
    return occupancyArray

def hogSVM():
    occupancyArray = []
    #insert code here
    return occupancyArray

def runMode(mode):
    if (mode == 0):
        measureMetrics()
    elif (mode == 1):
        sendOccupiedArray()
    elif (mode == 2):
        sendFeatures()
    elif (mode == 3):
        sys.exit(0)

def measureMetrics():
    global role
    image = cv2.imread("Lenna.png")
    win_size = (64, 128)
    image = cv2.resize(image, win_size)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    hog = cv2.HOGDescriptor()
    h = hog.compute(gray)
    num = 0

    if(role == "controller"):
        ping_array = []
        throughput_array = []
        accuracy_array = []

        print(' ************ NETWORK METRICS *********** ')
        print('1. Measuring ping')
        time_begin = time.time()
        data_to_send = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEF" #32 bytes
        counter = 0
        if(radio.write(data_to_send)):
            if(radio.isAckPayloadAvailable()):
                length = radio.getDynamicPayloadSize()
                received = radio.read(length)
                time_elapsed = time.time() - time_begin
                ping_array.append(time_elapsed*1000)
                print('    PING: {} ms'.format(time_elapsed*1000))

        else:
            print('Failed to ping node')
            mode = nrf.chooseMode()
            runMode(mode)

        print('2. Measuring throughput')
        data_to_send = json.dumps(h.tolist())
        data_size = len(data_to_send.encode('utf-8'))
        time_begin = time.time()
        if(not nrf.sendString(data_to_send)):
            mode = nrf.chooseMode()
            runMode(mode)
        time_elapsed = time.time() - time_begin
        throughput = float((data_size*8)/time_elapsed)/1000000
        throughput_array.append(throughput)
        print('    TIME: {} seconds'.format(time_elapsed))
        print('    SIZE: {} bytes'.format(data_size))
        print('    THROUGHPUT: {} Mbps'.format(throughput))
        
        print('3. Measuring accuracy (node operation)')
        received = nrf.recvString()
        print('{}'.format(received))

        mode = nrf.chooseMode()
        runMode(mode)

    if(role == "node"):
        while True:
            if(radio.available()):
                length = radio.getDynamicPayloadSize()
                received = radio.read(length)
                radio.writeAckPayload(1, "PING")
                print('received ping')
                break
        
        #Receive features to measure throughput
        received = nrf.recvString()
        print('received string')

        #format received string to array
        received_temp = np.array(json.loads(received))
        received_array = received_temp.flatten()
        #format in-node processed features to array
        temp = json.dumps(h.tolist())
        data_to_compare = np.array(json.loads(temp))
        stored_array = data_to_compare.flatten()
        #compare received array to stored array
        counter = 0
        error = 0
        for element in stored_array:
            if(not stored_array[counter]==received_array[counter]):
                error += 1
        data_to_send = "    ACCURACY: "+str(float((len(stored_array)-error)/len(stored_array))*100)+"%"
        nrf.sendString(data_to_send)

        mode = nrf.chooseMode()
        runMode(mode)

def sendStringLoop():
    global role
    while True:
        if (role == "node"):
            data_to_send = "NINETY MILES OUTSIDE CHICAGO CANT STOP DRIVING I DONT KNOW WHY SO MANY QUESTIONS I NEED AN ANSWER TWO YEARS LATER YOU STILL ON MY MIIIIIND"
            if(not nrf.sendString(data_to_send)):
                print('entered if')
                mode = nrf.chooseMode()
                runMode(mode)
            time.sleep(1)
        if (role == "controller"):
            received = nrf.recvString()
            print('Received string: {}'.format(received))

def sendFeatures():
    global role
    image = cv2.imread("Lenna.png")
    win_size = (64, 128)
    image = cv2.resize(image, win_size)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    hog = cv2.HOGDescriptor()
    h = hog.compute(gray)

    while True:
        if (role == "node"):
            data_to_send = json.dumps(h.tolist())
            print('feature: {}'.format(data_to_send))
            if(not nrf.sendString(data_to_send)):
                mode = nrf.chooseMode()
                runMode(mode)
            time.sleep(1)
        if(role == "controller"):
            received = nrf.recvString()
            print('received feature: {}'.format(received))    
            temp = np.array(json.loads(received))
            received_array = temp.flatten()
            for x in received_array:
                print('array element: {}'.format(x))

if __name__ == "__main__":

    nrf.config()
    inp_role, role = nrf.userDefineRoles()
    print('role: {}'.format(role))

    if (role == "controller"):
        nrf.findNodes()
    if (role == "node"):
        nrf.waitStartNormal()

    mode = nrf.chooseMode()
    #print('mode: {}'.format(mode))
    runMode(mode)
