#!/usr/bin/env python

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24 as nrf
import random

import cv2
import numpy as np
from base64 import b64decode, b64encode
import zlib
import time

role = "controller"
inp_role = '0'

nrf.config()
nrf.controller_config()

#node 1
nrf.config_to_node1()

nrf.sendPacket("INITIALIZE SLOTS 1")

ack_ping = nrf.recvPacket(1)
print('Ack : {}'.format(ack_ping))
if(ack_ping == "NODE 1"):
	print('*************************************')
	print('Now accepting picture from Node 1')
	time_begin = time.time()
	received = nrf.recvTCP(1)
	time_span = time.time() - time_begin
	print('time elapsed: {}'.format(time_span))
	decoded_data = b64decode(received)
	new_image_handle = open('parkinglot_node1.jpg', 'wb')
	new_image_handle.write(decoded_data)
	new_image_handle.close()

#node2
nrf.config_to_node2()
time.sleep(0.1)
nrf.sendPacket("INITIALIZE SLOTS 2")
ack_ping = nrf.recvPacket(2)
if(ack_ping == "NODE 2"):
	print('*************************************')
	print('Now accepting picture from Node 2')
	time_begin = time.time()
	received = nrf.recvTCP(2)
	decoded_data = b64decode(received)
	time_span = time.time() - time_begin
	print('time elapsed: {}'.format(time_span))
	new_image_handle = open('parkinglot_node2.jpg', 'wb')
	new_image_handle.write(decoded_data)
	new_image_handle.close()