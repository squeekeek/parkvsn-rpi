from __future__ import division

import os
import cv2
import xmltodict as xmld
import numpy as np
import math

from matplotlib import pyplot as plt

import img_math
import img_plot


def slice_one_img(img, rectpts):
    # Get each parking space by masking
    smask = np.zeros(img.shape, dtype=np.uint8)
    nchannels = img.shape[2] if (len(img.shape) == 3) else 1
    smask = cv2.fillPoly(smask, np.array([rectpts]), (255, ) * nchannels)
    pkspace = cv2.bitwise_and(smask, img)

    # Crop rectangles to masked size
    mincoords = map(min, zip(*rectpts))
    maxcoords = map(max, zip(*rectpts))
    normpts = map(tuple, np.subtract(rectpts, mincoords))

    return pkspace[mincoords[1]:maxcoords[1], mincoords[0]:maxcoords[0]]


def slice_img(img, xmlboxes):
    #print 'Reading dataset in parking lot ' + xmlboxes['parking']['@id']

    pspaces = []
    pspacepts = []
    for eachspace in xmlboxes['parking']['space']:
        rectpts = img_math.sortfourcw([(int(a['@x']), int(a['@y'])) for a in eachspace['contour']['point']])

        # Slice single rectangle from image
        new_img = slice_one_img(img, rectpts)
        pspaces.append(new_img)
        pspacepts.append([int(eachspace['@occupied']) if '@occupied' in eachspace else -1, rectpts.tolist()])

    #print 'Read ' + str(len(xmlboxes['parking']['space'])) + ' parking spaces successfully!'
    return [pspaces, pspacepts]


def main():
    with open('../datasets/EEEI/eeei_5th_solar_20170310/20170311-1626.xml') as fd:
        xmlf = xmld.parse(fd.read())
        img = cv2.imread('../datasets/EEEI/eeei_5th_solar_20170310/20170311-1626.jpg', 1)
        pspaces, prect = slice_img(img, xmlf)

        img_plot.plot_img_with_segments(img, prect)
        img_plot.plot_spaces(pspaces)

    plt.show()


if __name__ == "__main__":
    main()