from __future__ import division

import cv2
from matplotlib import pyplot as plt
import numpy as np

def plot_spaces(pspaces):
    # Plot each parking space
    plt.figure()
    plt.title('Segmented Parking Spaces')
    sqsize = np.ceil(np.sqrt(len(pspaces)))

    for k, eachimg in enumerate(pspaces):
        plt.subplot(sqsize, sqsize, k + 1)
        plt.title(str(k + 1), size=10, color='b', y=-0.01, weight='bold')
        plt.xticks([]), plt.yticks([])

        if len(eachimg.shape) == 3 and eachimg.shape[2] == 3:
            plt.imshow(eachimg[:, :, ::-1])
        else:
            plt.imshow(eachimg, 'gray')

    plt.subplots_adjust(wspace=0.05, hspace=0.05)


def plot_img_with_segments(img, ppts):
    # Show original image
    # Show with boxes
    plt.figure()
    plt.title('Original with ROI', fontsize=10)

    try:
        if len(img[0][0]) == 3:
            plt.imshow(img[:, :, ::-1])
        elif len(img[0][0]) < 3:
            plt.imshow(img, 'gray')
    except:
        plt.imshow(img, 'gray')

    plt.xticks([]), plt.yticks([])

    for k, (isfull, plotpts) in enumerate(ppts):
        plt.text(plotpts[0][0], plotpts[0][1], str(k + 1), size=10, color='b', weight='bold')

        for p in zip(plotpts + [plotpts[0]], \
                plotpts[1:] + [plotpts[0]]):
            if isfull == -1:
                plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'b-', linewidth=1)
            elif isfull == 0:
                plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'g-', linewidth=1)
            else:
                plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'r-', linewidth=1)


def write_seg_img(dirname, basename, pspaces):
    for k, eachimg in enumerate(pspaces):
        cv2.imwrite("{}/{}_seg{}.jpg".format(dirname, basename, k + 1), eachimg)