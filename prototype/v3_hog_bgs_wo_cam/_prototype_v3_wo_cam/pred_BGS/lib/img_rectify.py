from __future__ import division

import cv2
import math
import numpy as np

from matplotlib import pyplot as plt
from operator import itemgetter

import img_math


def get_affine_mat(alphabeta):
    if np.isclose(alphabeta[1], 0, atol=1e-6):
        return None

    affmat = np.eye(3)
    affmat[0, 0] = 1 / alphabeta[1]
    affmat[0, 1] = -alphabeta[0] / alphabeta[1]

    return affmat


def get_vanishing_point(pts):
    u"""
    From The 3-Point Method: A Fast, Accurate and Robust Solution to
    Vanishing Point Estimation

    A vanishing point is calculated using the algorithm developed by Saini
    et al (2013)
    """
    a, c, b = pts

    alp = ((c[0] - a[0]) * (0.5 - 1)) / ((c[0] - b[0]) * 0.5)
    vp_x, vp_y = (alp / (alp - 1)) * np.array([b[0] - a[0], b[1] - a[1]])

    return np.array([vp_x, vp_y, 1])


def get_new_pdim(img_rect, pmat):
    img_pts = np.float32(img_rect).reshape(-1, 1, 2)
    img_pts_p = cv2.perspectiveTransform(img_pts, pmat)

    [xmin, ymin] = np.int32(img_pts_p.min(axis=0).ravel() - 0.5)
    [xmax, ymax] = np.int32(img_pts_p.max(axis=0).ravel() + 0.5)

    tvec = [-xmin, -ymin]
    Ht = np.array([[1, 0, tvec[0]], [0, 1, tvec[1]], [0, 0, 1]])

    return Ht, (xmax - xmin, ymax - ymin)


def get_new_adim(dim, amat):
    h, w = dim

    img_pts = np.float32([[0, 0], [0, h], [w, h], [w, 0]]).reshape(-1, 1, 2)
    img_pts_a = cv2.transform(img_pts, amat)

    [xmin, ymin] = np.int32(img_pts_a.min(axis=0).ravel() - 0.5)
    [xmax, ymax] = np.int32(img_pts_a.max(axis=0).ravel() + 0.5)

    tvec = [-xmin, -ymin]
    Ht = np.array([[1, 0, tvec[0]], [0, 1, tvec[1]], [0, 0, 1]])

    return Ht, (xmax - xmin, ymax - ymin)


def correct_perspective(img, vert_pts, horiz_pts):
    """Correct perspective of image and show all visible pixels

    Lines made by vert_pts and horiz_pts should be perpendicular with each
    other in the world space. The points should be collinear.

    :param img: image to correct the perspective
    :param vert_pts: 3-element list of points in the general vertical diretion
    :param horiz_pts: 3-element list of points in the general horizontal
                      direction
    """
    vert_pts_pos = zip(*vert_pts)
    horiz_pts_pos = zip(*horiz_pts)

    # Fit points to a line via least squares
    line_mxb_horiz, line_mxb_vert = {}, {}
    line_mxb_horiz["m"], line_mxb_horiz["b"] = np.linalg.lstsq(np.vstack([horiz_pts_pos[0], np.ones(len(horiz_pts_pos[0]))]).T, horiz_pts_pos[1])[0]
    line_mxb_vert["m"], line_mxb_vert["b"] = np.linalg.lstsq(np.vstack([vert_pts_pos[0], np.ones(len(vert_pts_pos[0]))]).T, vert_pts_pos[1])[0]

    # Get vanishing points
    horiz_lstsq = img_math.get_closest_to_line(horiz_pts, line_mxb_horiz)
    vp_horiz = get_vanishing_point(horiz_lstsq)

    vert_lstsq = img_math.get_closest_to_line(vert_pts, line_mxb_vert)
    vp_vert = get_vanishing_point(vert_lstsq)

    # Set-up perspective matrix
    linf = img_math.linepts_to_abc([vp_vert, vp_horiz])
    pmat = np.vstack([np.eye(2, 3), linf])

    # Transform image
    Ht, new_dim = get_new_pdim([[0, 0], [0, img.shape[0]], [img.shape[1], img.shape[0]], [img.shape[1], 0]], pmat)
    img_p = cv2.warpPerspective(img, Ht.dot(pmat), new_dim)

    return img_p, Ht.dot(pmat), (vp_horiz, vp_vert)


def correct_affine(img, linelist):
    """Correct affinity of image and show all visible pixels

    :param img: image to correct the perspective
    :param linelist: list of four lines as reference. The first two should be
                     be perpendicular to each other. Same should be the case
                     for the last two.
    """
    # Transform points
    circ_a = img_math.linepair_to_circle_90deg(linelist[:2])
    circ_b = img_math.linepair_to_circle_90deg(linelist[2:])

    '''
    #print circ_a, circ_b

    plt.figure()
    ax = plt.gcf().gca()
    ax.set_xlim((-100, 100))
    ax.set_ylim((-100, 100))
    ax.add_artist(plt.Circle(circ_a[0], circ_a[1], color='r', fill=False))
    ax.add_artist(plt.Circle(circ_b[0], circ_b[1], color='g', fill=False))
    '''

    circ_intersect = img_math.circle_intersect(np.array([circ_a, circ_b]))
    amat = get_affine_mat(circ_intersect)

    #amat = np.eye(3)
    Ht, new_dim = get_new_pdim(img.shape[:2], amat)
    newimg = cv2.warpPerspective(img, Ht.dot(amat), new_dim)

    # Returns corrected image
    return newimg, Ht.dot(amat)


def correct_similarity(img, rot_arg):
    # Get angle
    xvec = np.array([[0, img_color.shape[0]], img_color.shape[1::-1]])
    xvec_p = cv2.perspectiveTransform(np.float32([xvec]), pmat)
    xvec_a = cv2.perspectiveTransform(xvec_p, amat)[0]
    xvec_arg = math.degrees(math.acos(np.dot(xvec_a[1] - xvec_a[0], xvec[1] - xvec[0]) / (np.linalg.norm(xvec_a[1] - xvec_a[0]) * img_color.shape[1])))

    if rot_arg > 90:
        rot_arg -= 90
    elif rot_arg < -90:
        rot_arg += 90

    rmat = np.vstack((cv2.getRotationMatrix2D(tuple(xvec_a[0]), -xvec_arg, 1), [0, 0, 1]))

    Ht, rdim = get_new_pdim(img.shape[:2], rmat)
    img_r = cv2.warpPerspective(img, Ht.dot(rmat), rdim)

    return img_r, Ht.dot(rmat)

def main():
    '''
    img_name = '../datasets/EEEI/eeei_5th_solar_20170329/20170330-0545.jpg'

    vert_pts = np.array([(433, 490), (473, 414), (512, 339)])
    horiz_pts = np.array([(546, 532), (389, 515), (234, 492)])
    line_vec = np.float32([[(433, 490), (512, 339)],
                           [(234, 492), (546, 532)],
                           [(804, 425), (863, 351)],
                           [(863, 351), (922, 400)]])
    '''

    #'''
    img_name = '../datasets/EEEI/eeei_cnl_20170317/20170318-1638.jpg'

    vert_pts = np.array([(490, 513), (461, 594), (433, 675)])
    horiz_pts = np.array([(275, 513), (382, 513), (490, 513)])

    line_vec = np.float32([[(186, 619), (271, 519)],
                           [(269, 518), (487, 522)],
                           [(454, 620), (593, 523)],
                           [(593, 523), (711, 586)]])
    #'''

    '''
    img_name = '../datasets/EEEI/eeei_5th_solar_20170414/20170414-1549.jpg'

    vert_pts = np.array([(899, 499), (889, 569), (880, 651)])
    horiz_pts = np.array([(832, 571), (891, 570), (952, 566)])

    line_vec = np.float32([[(850, 502), (814, 655)],
                           [(846, 503), (954, 492)],
                           [(836, 569), (896, 499)],
                           [(895, 497), (953, 544)]])
    '''

    img_color = cv2.imread(img_name, 1)

    # Perspective transform
    img_p, pmat, img_vpts = correct_perspective(img_color, vert_pts, horiz_pts)
    p_lines = cv2.perspectiveTransform(line_vec, pmat)

    # Affine transform
    img_a, amat = correct_affine(img_p, p_lines)
    a_lines = cv2.perspectiveTransform(p_lines, amat)

    # Similarity transform
    # Get angle
    xvec = np.array([[0, img_color.shape[0]], img_color.shape[1::-1]])
    xvec_p = cv2.perspectiveTransform(np.float32([xvec]), pmat)
    xvec_a = cv2.perspectiveTransform(xvec_p, amat)[0]
    xvec_arg = math.degrees(math.acos(np.dot(xvec_a[1] - xvec_a[0], xvec[1] - xvec[0]) / (np.linalg.norm(xvec_a[1] - xvec_a[0]) * img_color.shape[1])))

    if xvec_arg > 90:
        xvec_arg -= 90
    elif xvec_arg < -90:
        xvec_arg += 90

    linea = img_math.linepts_to_abc(a_lines[0])
    rmat = np.vstack((cv2.getRotationMatrix2D(tuple(xvec_a[0]), -xvec_arg, 1), [0, 0, 1]))

    Ht, rdim = get_new_pdim(img_a.shape[:2], rmat)
    img_r = cv2.warpPerspective(img_a, Ht.dot(rmat), rdim)

    plt.figure()
    plt.plot(zip(*vert_pts)[0], zip(*vert_pts)[1], 'r-')
    plt.plot(zip(*horiz_pts)[0], zip(*horiz_pts)[1], 'g-')

    for d in line_vec:
        a, b = zip(*d)
        plt.plot(a, b, 'b-')

    plt.imshow(img_color[:, :, ::-1])

    plt.figure()
    for d in p_lines:
        a, b = zip(*d)
        plt.plot(a, b, 'b-')

    plt.imshow(img_p[:, :, ::-1])

    plt.figure()
    for d in a_lines:
        a, b = zip(*d)
        plt.plot(a, b, 'b-')
    plt.imshow(img_a[:, :, ::-1])

    plt.figure()
    plt.imshow(img_r[:, :, ::-1])

    plt.show()

if __name__ == "__main__":
    main()
