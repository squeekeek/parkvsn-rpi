from __future__ import division
import os
import cv2
import numpy as np
import xmltodict as xmld
import matplotlib    
from matplotlib import pyplot as plt
from operator import itemgetter

def thresh(myimg):
    pass

def sortfourcw(rectpts):
    crossprod = lambda a, b: a[0] * b[1] - a[1] * b[0]
    ccwtest = lambda a, b, c: crossprod(a, b) + crossprod(b, c) + crossprod(c, a)

    if len(rectpts) != 4:
        return None

    a, b, c, d = rectpts
    
    if ccwtest(a, b, c) < 0.0:
        if ccwtest(a, c, d) < 0.0:
            pass
        elif ccwtest(a, b, d) < 0.0:
            d, c = c, d
        else:
            d, a = a, d
        pass
    elif ccwtest(a, c, d) < 0.0:
        if ccwtest(a, b, d) < 0.0:
            c, b = b, c
        else:
            b, a = a, b
    else:
        c, a = a, c
    
    ccwpts = [a, b, c, d]
    ccwpts = np.roll(ccwpts, -np.argmin(np.sum(ccwpts, axis=1)), axis=0)
    
    return np.array(ccwpts)
    
def transspace(myimg, rectpts):
    rectpts = np.array(rectpts)
    
    # Correct rotation
    rectctr, rectdim, rectarg = np.array(cv2.minAreaRect(rectpts))
    if rectdim[0] > rectdim[1]:
        rectarg += 90
    
    # Transform points
    rotmat = cv2.getRotationMatrix2D(rectctr, rectarg, 1)
    s = np.array(map(lambda t: tuple(t) + (1, ), rectpts))
    rectpts = np.transpose(np.matmul(rotmat, np.transpose(s)))
    
    destsize = np.array(cv2.minAreaRect(np.int32(rectpts)))[1]
    rectdim = map(int, (destsize[0], destsize[1]) if destsize[0] < destsize[1] else (destsize[1], destsize[0]))
    deltax, deltay = rectdim[0] / 2.0 - rectctr[0], rectdim[1] / 2.0 - rectctr[1]
    
    rotmat[0][2] += deltax
    rotmat[1][2] += deltay
    rectpts = np.float32(map(lambda pt: (pt[0] + deltax, pt[1] + deltay), rectpts))
    imgrotd = cv2.warpAffine(myimg, rotmat, tuple(rectdim))
    
    # Correct perspective
    ccwpts = sortfourcw(rectpts)
    newpts = np.float32([(0, 0), (0, rectdim[1]), rectdim, (rectdim[0], 0)])
    
    perspmat = cv2.getPerspectiveTransform(ccwpts, newpts)
    newimg = cv2.warpPerspective(imgrotd, perspmat, tuple(rectdim))
    
    # Returns corrected image
    return newimg

def slice_img(img, xmlboxes):
    print 'Reading dataset in parking lot ' + xmlboxes['parking']['@id']
    
    pspaces = []
    pspacepts = []
    for eachspace in xmlboxes['parking']['space']:
        rectpts = sortfourcw([(int(a['@x']), int(a['@y'])) for a in eachspace['contour']['point']])
        
        # Get each parking space by masking
        smask = np.zeros(img.shape, dtype=np.uint8)
        nchannels = img.shape[2] if (len(img.shape) == 3) else 1
        smask = cv2.fillPoly(smask, np.array([rectpts]), (255, ) * nchannels)
        pkspace = cv2.bitwise_and(smask, img)
        
        # Crop rectangles to masked size
        mincoords = map(min, zip(*rectpts))
        maxcoords = map(max, zip(*rectpts))
        normpts = map(tuple, np.subtract(rectpts, mincoords))
        
        # Then rotate and perspective transform
        pspaces.append(transspace(pkspace[mincoords[1]:maxcoords[1], mincoords[0]:maxcoords[0]], normpts))
        
        pspacepts.append((int(eachspace['@occupied']) if '@occupied' in eachspace else -1, rectpts.tolist()))
    
    print 'Read ' + str(len(xmlboxes['parking']['space'])) + ' parking spaces successfully!'
    return (pspaces, pspacepts)

def plot_spaces(pspaces):
    # Plot each parking space
    plt.figure()
    plt.title('Segmented Parking Spaces')
    sqsize = np.ceil(np.sqrt(len(pspaces)))
    for k, eachimg in enumerate(pspaces):
    
        plt.subplot(sqsize, sqsize, k + 1)
        plt.title(str(k + 1), size=8, color='b', y=-0.01, weight='bold')
        plt.xticks([]), plt.yticks([])
        plt.imshow(eachimg, 'gray')
        #plt.imshow(eachimg[:,:,::-1])
    
    plt.subplots_adjust(wspace = 0.05, hspace = 0.05)

def plot_img_with_segments(img, ppts):
    # Show original image
    # Show with boxes
    plt.figure()
    plt.title('Original with ROI', fontsize=10)
    plt.imshow(img[:, :, ::-1])
    plt.xticks([]), plt.yticks([])
    
    for k, (isfull, plotpts) in enumerate(ppts):
        plt.text(plotpts[0][0], plotpts[0][1], str(k + 1), size=6, color='b', weight='bold')
        for p in zip(plotpts + [plotpts[0]], \
                plotpts[1:] + [plotpts[0]]):
            if isfull == -1:
                plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'b-', linewidth=1)
            elif isfull == 0:
                plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'g-', linewidth=1)
            else:
                plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'r-', linewidth=1)
    
def main():
    #filename = '../datasets/PKLot/EEEI/crop_830_1'
    #filename = '../datasets/PKLot/EEEI/crop_2pm3'
    #filename = '../datasets/PKLot/PUCPR/Cloudy/2012-09-12/2012-09-12_07_23_35'
    #filename = '../datasets/PKLot/UFPR04/Cloudy/2013-01-16/2013-01-16_09_25_05'
    #filename = '../datasets/PKLot/UFPR04/Cloudy/2013-01-16/2013-01-16_09_25_05'
    #filename = '../datasets/PKLot/PUCPR/Sunny/2012-09-11/2012-09-11_15_55_21'
    #filename = '../datasets/PKLot/UFPR05/Rainy/2013-03-19/2013-03-19_12_50_07'
    #filename = '../datasets/EEEI/eeei_5th_solar/20170313-1346'
    #filename = '../datasets/EEEI/eeei_5th_solar/20170311-1936'
    #filename = '../datasets/EEEI/eeei_5th_solar/20170311-1626'
    
    '''
    with open(filename + '.xml') as fd:
        doc = xmld.parse(fd.read())
        
        print 'Reading filename ' + filename
        img = cv2.imread(filename + '.jpg', 1)
        img_thresh = cv2.adaptiveThreshold(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 5, 3)
        pspaces, ppts = slice_img(img, doc)
        pthresh, _ = slice_img(img_thresh, doc)
        
        # Show original image
        # Show with boxes
        plt.figure()
        plt.title('Original with ROI', fontsize=10)
        plt.imshow(img[:, :, ::-1])
        plt.xticks([]), plt.yticks([])
        
        for k, (isfull, plotpts) in enumerate(ppts):
            plt.text(plotpts[0][0], plotpts[0][1], str(k + 1), size=6, color='b', weight='bold')
            for p in zip(plotpts + [plotpts[0]], \
                    plotpts[1:] + [plotpts[0]]):
                if isfull == -1:
                    plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'b-', linewidth=1)
                elif isfull == 0:
                    plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'g-', linewidth=1)
                else:
                    plt.plot((p[0][0], p[1][0]), (p[0][1], p[1][1]), 'r-', linewidth=1)
        
        # Plot each parking space
        plt.figure()
        plt.title('Segmented Parking Spaces')
        sqsize = np.ceil(np.sqrt(len(pspaces)))
        for k, eachimg in enumerate(pspaces):
        #for k, eachimg in enumerate(pthresh):
            #grayimg = cv2.cvtColor(eachimg, cv2.COLOR_BGR2GRAY)
            #edgeimg = cv2.Canny(eachimg, 75, 100)
            #threshimg = cv2.adaptiveThreshold(grayimg, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 5, 3)
            #threshimg = cv2.morphologyEx(edgeimg, cv2.MORPH_CLOSE, np.ones([3, 3]))
        
            plt.subplot(sqsize, sqsize, k + 1)
            plt.title(str(k + 1), size=8, color='b', y=-0.01, weight='bold')
            plt.xticks([]), plt.yticks([])
            #plt.imshow(eachimg, 'gray')
            plt.imshow(eachimg[:,:,::-1])
            #plt.imshow(threshimg, 'gray')
            #plt.imshow(edgeimg, 'gray')
        
        plt.subplots_adjust(wspace = 0.05, hspace = 0.05)
    '''
    #dataset_path = '../datasets/PKLot/PUCPR/Cloudy/2012-09-12/'
    #dataset_path = '../datasets/EEEI/eeei_5th_solar/'
    #dataset_path = '../datasets/PKLot/UFPR04/Cloudy/2013-01-16/'
    dataset_path = "dataset_test/test_blanks"
    
    fnames = [f for f in os.listdir(dataset_path) if f.endswith('.jpg')]
    bgsub = cv2.createBackgroundSubtractorMOG2()

    for k, eachfile in enumerate(fnames):
        if k < 20:
            continue
    
        if k >= 25:
            break
    
        imgname = os.path.splitext(eachfile)[0]
        filename = dataset_path + imgname
        
        with open(filename + '.xml') as fd:
            doc = xmld.parse(fd.read())
            img = cv2.imread(filename + '.jpg', 1)
            img_thresh = cv2.adaptiveThreshold(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 5, 3)
            
            fgmask = bgsub.apply(img_thresh)
            fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_CLOSE, np.ones([5, 5]))
            pspaces, ppts = slice_img(fgmask, doc)
            
            plot_img_with_segments(img, ppts)
            #cv2.imwrite("sample.jpg", img_thresh)
            
            plot_spaces(pspaces)
        
    # Show all plots
    plt.show()

if __name__ == "__main__":
    main()