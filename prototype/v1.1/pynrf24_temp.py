#!/usr/bin/env python

#
# PyNRF24 Boilerplate
#
from __future__ import print_function
import time
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import hashlib

import logging
import sys

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s (%(threadName)-2s) %(message)s')


irq_gpio_pin = None

interrupt_stop = False

# The write addresses of the peripheral nodes
addr_central_rd = [0xF0F0F0F0C1, 0xF0F0F0F0D1, 0xF0F0F0F0E1, 0xF0F0F0F0F1, 0xF0F0F0F0A1]

# The read addresses of the peripheral nodes
addr_central_wr = [0xF0F0F0F0AB, 0xF0F0F0F0BC, 0xF0F0F0F0DE, 0xF0F0F0F0FA, 0xF0F0F0F0BA]

inp_role = 'none'

#Timeout in seconds for the controller to receive msg from node
timeout = 5

# to accomodate for the 5 reading pipes/nodes, not including the first one
found_nodes = [0, 0, 0 ,0 ,0 ,0]

role = 'none'

radio = RF24(17, 0)

millis = lambda: int(round(time.time() * 1000))

def chooseMode():
    print('inside choose mode')
    global role, inp_mode
    inp_mode = 'none'
    if(role == "controller"):
        print(' ************ Choose Mode *********** ')
        while (inp_mode !='0') and (inp_mode !='1') and (inp_mode !='2') and (inp_mode != '3'):
            inp_mode = str(input('Choose a mode: \n 0: Measure metrics \n 1: Send occupied array \n 2: Send features \n 3: exit \n'))
        radio.stopListening()
        counter = 0
        data_to_send = inp_mode
        while(counter < 10):
            radio.write(data_to_send)
            if(radio.isAckPayloadAvailable()):
                length = radio.getDynamicPayloadSize()
                received = radio.read(length)
                if(received.decode('utf-8')=="MODE"):
                    #print('wrote mode')
                    break
            time.sleep(0.1)
            counter += 1
        #print('outside loop')
        if (inp_mode == '0'):
            return 0
        elif (inp_mode == '1'):
            return 1
        elif (inp_mode == '2'):
            return 2
        elif (inp_mode == '3'):
            #print('inside')
            sys.exit(0)

    if(role == "node"):
        while True:
            radio.startListening()
            if(radio.available()):
                length = radio.getDynamicPayloadSize()
                received = radio.read(length)
                radio.writeAckPayload(1, "MODE")
                print('wrote ack payload mode ata')
                if(received.decode('utf-8')=='0'):
                    print('received 0')
                    return 0
                elif(received.decode('utf-8')=='1'):
                    print('received 1')
                    return 1
                elif(received.decode('utf-8')=='2'):
                    print('received 2')
                    return 2
                elif(received.decode('utf-8')=='3'):
                    sys.exit(0)

def sendPacket(send_packet):
    global radio
    radio.stopListening()
    radio.flush_tx()
    while True:
        if(radio.write(send_packet)):
            timeout = False
            time_start = millis()
            while(not radio.isAckPayloadAvailable() and (not timeout)):
                if(millis()-time_start > 500):
                    #print('timeout')
                    timeout = True
            if(not timeout):
                length = radio.getDynamicPayloadSize()
                received = radio.read(length)
                #print('received ack payload: {}'.format(received.decode('utf-8')))
                if(send_packet == received.decode('utf-8')):
                    to_send = "ACK"
                    if(radio.write(to_send)):
                        #print('wrote good')
                        break
                else:
                    to_send = "SEND AGAIN"
                    radio.write(to_send)
        time.sleep(0.0001)

def recvPacket():
    global radio

    radio.startListening()
    while True:
        if(radio.available()):
            length = radio.getDynamicPayloadSize()
            received = radio.read(length)
            radio.writeAckPayload(1, received)
            #print('received packet: {}'.format(received.decode('utf-8')))
            timeout = False
            time_start = millis()
            while(not radio.available() and (not timeout)):
                if(millis()-time_start > 500):
                    #print('timeout')
                    timeout = True
            if(not timeout):
                length = radio.getDynamicPayloadSize()
                received_ack = radio.read(length)
                #print('response packet: {}'.format(received_ack.decode('utf-8')))
                if(received_ack.decode('utf-8')=="ACK"):
                    return received.decode('utf-8')
            else:
                pass
        else:
            pass

def sendTCP(data_to_send):
    packet_list = parsePacket(data_to_send)
    for num in range(len(packet_list)):
        print('NOW SENDING: {}'.format(packet_list[num]))
        sendPacket(packet_list[num])
    sendPacket("DONE SENDING STRING")

def recvTCP():
    recv_buffer = []

    while True:
        received = recvPacket()
        if(received=="DONE SENDING STRING"):
            whole_string = ''.join(recv_buffer)
            break
        else:
            recv_buffer.append(received)
            print('RECEIVED: {}'.format(received))

    return whole_string

def parsePacket(data_to_send):
    """
        Accepts string
        Returns list of 32 byte strings (if applicable)
        For sending packets
    """
    packet_list = []
    len_data = len(data_to_send)

    i = 0
    while (1):
        starting = i*32
        if (len_data <= 32):
            end = starting + len_data
            packet_list.append(data_to_send[starting:end])
            break
        else:
            end = starting + 32
            packet_list.append(data_to_send[starting:end])
            len_data = len_data - 32
        i = i + 1

    return packet_list

def sendString(data_to_send):

    global radio, interrupt_stop

    radio.stopListening()
    packet_list = parsePacket(data_to_send)
    radio.flush_tx()
    for num in range(len(packet_list)):
        #print('now sending packet: {}'.format(num))
        radio.write(packet_list[num])
        counter = 0
        while(counter < 5):
            if(radio.isAckPayloadAvailable()):
                length = radio.getDynamicPayloadSize()
                received = radio.read(length)
                if(received.decode('utf-8') == "STOP"):
                    print(' *********** RECEIVED STOP *************')
                    interrupt_stop = True
                    break
                else:
                    break
            time.sleep(0.0001)
            counter += 1
    string_byte = "DONE SENDING STRING"
    while True:
        if(interrupt_stop):
            interrupt_stop = False
            return False
        if(radio.write(string_byte)):
            radio.startListening()
            return True


def recvString():
    """
    returns string received from sendString() function
    """
    global radio
    recv_buffer = []

    radio.startListening()
    while True:
        while( not radio.available()):
            time.sleep(0.0001)
        length = radio.getDynamicPayloadSize()
        received = radio.read(length)
        if(received.decode('utf-8')=="DONE SENDING STRING"):
            whole_string = ''.join(recv_buffer)
            break
        else:
            radio.writeAckPayload(1,"ack")
            recv_buffer.append(received.decode('utf-8'))
            #print('received packet: {}'.format(received.decode('utf-8')))

    return whole_string


def config():
    global radio
    # Radio Number: GPIO number, SPI bus 0 or 1
    radio.begin()
    radio.enableAckPayload()
    radio.setAutoAck(True)
    radio.enableDynamicPayloads()
    radio.setRetries(5,15)
    radio.setPALevel(RF24_PA_MIN) #if close together
    radio.printDetails()


def userDefineRoles():
    """
        Description:
            CLI based user input roles
            Can be extended to 5 peripheral nodes by copy-pasting
        Return:
            Tuple: (inp_role, role)
    """
    global inp_role, radio, camera, rawCapture, role

    print(' ************ Role Setup *********** ')
    while (inp_role !='0') and (inp_role !='1') and (inp_role !='2'):
        inp_role = str(input('Choose a role: Enter 0 for controller, 1 for node1, 2 for node2 CTRL+C to exit) '))


    if (inp_role == '0'):
        print('Role: Controller, starting transmission')
        radio.openReadingPipe(1, addr_central_rd[0])
        radio.openReadingPipe(2, addr_central_rd[1])
        time.sleep(1)
        # TODO: can insert up to 5 readng pipes
        role = "controller"
        counter = 0

    elif (inp_role == '1'):
        # HAS OPENCV
        print('Role: node1 to be accessed, awaiting transmission')
        radio.openWritingPipe(addr_central_rd[0])
        radio.openReadingPipe(1, addr_central_wr[0])
        time.sleep(1)
        role = "node"
        counter = 0

    elif (inp_role == '2'):
        print('Role: node2 to be accessed, awaiting transmission')
        radio.openWritingPipe(addr_central_rd[1])
        radio.openReadingPipe(2,addr_central_wr[1])
        time.sleep(1)
        role = "node"
        counter = 0

    radio.startListening()
    return (inp_role, role)

def findNodes():

    global radio, addr_central_wr
    counter = 0

    #Controller tests node 1
    radio.openWritingPipe(addr_central_wr[0])
    radio.stopListening()
    time.sleep(1)

    #write to node 1
    data_to_send = "Found Node 1"
    while(counter < 4):
        if(radio.write(data_to_send)):
            print('Written to Node 1')
            found_nodes[0]=1
            break
        else:
            counter += 1
        time.sleep(1)
    if(counter == 4):
        print('Did not find Node 1')
        radio.closeReadingPipe(1)

    radio.openWritingPipe(addr_central_wr[1])
    counter = 0

    #write to node 2
    data_to_send = "Found Node 2"
    while(counter < 4):
        if(radio.write(data_to_send)):
            print('Written to Node 2')
            found_nodes[1]=1
            break
        else:
            counter += 1
        time.sleep(1)
    if(counter == 4):
        print('Did not find Node 2')
        radio.closeReadingPipe(2)

    for num in range(len(found_nodes)):
        if(found_nodes[num]==1):
            radio.openWritingPipe(addr_central_wr[num])

def waitStartNormal():

    global radio

    radio.startListening()

    while True:
        if(radio.available()):
            length = radio.getDynamicPayloadSize()
            received = radio.read(length)
            print('Received: {}'.format(received.decode('utf-8')))
            break

def restartRadio():
    radio.failureDetected = 0
    radio.begin()

    if (inp_role == '0'):
        print('Role: Controller, starting transmission')
        radio.openReadingPipe(1, addr_central_rd[0])
        radio.openReadingPipe(2, addr_central_rd[1])
        radio.openWritingPipe(addr_central_wr[0])
        radio.openWritingPipe(addr_central_wr[1])
        time.sleep(1)
        radio.stopListening()

    elif (inp_role == '1'):
        # HAS OPENCV
        print('Role: node1 to be accessed, awaiting transmission')
        radio.openWritingPipe(addr_central_rd[0])
        radio.openReadingPipe(1, addr_central_wr[0])
        time.sleep(1)
        radio.startListening()

    elif (inp_role == '2'):
        print('Role: node2 to be accessed, awaiting transmission')
        radio.openWritingPipe(addr_central_rd[1])
        radio.openReadingPipe(1,addr_central_wr[1])
        time.sleep(1)
        radio.startListening()