#!/usr/bin/env python



import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

from datetime import datetime
#import improc_lib
from p1_hog_svm import hog_svm

DEBUG = 1

if __name__ == "__main__":
    # STATE 1: SEGMENT IMAGES INTO PARKING LOT SPACES
    # INSERT CODE FOR SEGMENTING IMAGES
    # TEMP CODE
    image_folder = "dataset_test/eeei_5th_solar"
    image_paths = [f for f in os.listdir(image_folder) if f.endswith('.jpg')]

    # Pakipalitan si filename. :D
    """
    img_name = '20170313-1346'
    eachspace = []

    with open('dataset_test/' + img_name + '.xml') as fd:
        xmlh = xmld.parse(fd.read())
        test_img = cv2.imread('dataset_test/' + img_name + '.jpg', 1)

        pspaces, ppts = improc_lib.slice_img(test_img, xmlh)
        eachspace.append(pspaces)
    """
    # STATE 2: BACKGROUND THRESHOLDING
    # TEMP CODE
    """
    BT_result = []
    for i in range(0, len(eachspace), 1):
        BT_result.append(0)
    """

    #STATE2: HOG + SVM

    # Load json for models to use 
    json_path = "p1_hog_svm_models.json"
    with open(json_path) as data_file:
        json_dict = json.load(data_file)

    HOG_result = []
    for i in range(0, len(image_paths), 1):
        HOG_result.append(0)

    #for i, image in enumerate(image_paths):
    total_time = 0
    predict_time = 0
    for i, image_path in enumerate(image_paths):
        actual_path = os.path.join(image_folder, image_path)
        if DEBUG:
            print actual_path
        image = cv2.imread(actual_path)
        start_time = datetime.now()
        predict = hog_svm(image, json_dict)
        end_time = datetime.now()
        predict_time = end_time - start_time
        print ("Prediction time: {}").format(predict_time)
        parking_lot_no = image_path.split("seg")[-1].split(".")[0]
        parking_lot_num = int(parking_lot_no)
        HOG_result[parking_lot_num-1] = predict

    #print ("Background Thresolding: {}").format(str(BT_result))
    print ("HoG + SVM: {}").format(str(HOG_result))

    parking_location = "EEEI Student's Parking Lot"
    print ("###############################")
    print ("")
    print ("Parking Lot: {}").format(parking_location)
    print ("")
    for i, occupancy in enumerate(HOG_result):
        if HOG_result[i] == 1:
            state = "OCCUPIED"
        else:
            state = "EMPTY"
        print ("#{}: {}").format(i + 1, state)
    print ("")
    print ("###############################")
