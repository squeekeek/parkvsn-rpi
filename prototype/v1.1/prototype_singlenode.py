#!/usr/bin/env python



import cv2
import numpy as np

import logging


from skimage import exposure
from skimage import feature
from sklearn.externals import joblib

import argparse
import glob
import os
import json
import io
import sys
import imutils
import xmltodict as xmld

import improc_lib
from p1_hog_svm import hog_svm

if __name__ == "__main__":
    # STATE 1: SEGMENT IMAGES INTO PARKING LOT SPACES
    # INSERT CODE FOR SEGMENTING IMAGES
    # TEMP CODE
    # image_folder = "eeei_5th_solar"
    # image_paths = [f for f in os.listdir(image_folder) if f.endswith('.jpg')]

    # Pakipalitan si filename. :D
    img_name = '20170313-1206'
    #img_name = "dataset_test/eeei_5th_solar_dataset/20170311-1636.jpg"
    #img_name = "/Users/agatha/Documents/CoE198/parkvsn/prototype/v1.1/dataset_test/eeei_5th_solar_dataset/20170311-1736.jpg" #<== may unoccupied
    pspaces, ppts, test_img = None, None, None

    with open('dataset_test/' + "20170313-1206" + '.xml') as fd:
        xmlh = xmld.parse(fd.read())
    print img_name
    #test_img = cv2.imread(img_name, 1)
    test_img = cv2.imread('dataset_test/' + img_name + '.jpg', 1)
    pspaces, ppts = improc_lib.slice_img(test_img, xmlh)
        #plot_img_with_segments(test_img, ppts)
    # STATE 2: BACKGROUND THRESHOLDING
    # TEMP CODE
    """
    BT_result = []
    for i in range(0, len(pspaces), 1):
        BT_result.append(0)
    """

    #STATE2: HOG + SVM

    # Load json for models to use 
    json_path = "p1_hog_svm_models.json"
    with open(json_path) as data_file:
        json_dict = json.load(data_file)

    HOG_result = []

    for i, eachspace in enumerate(pspaces):
        #cv2.imshow("d", eachspace)
        #cv2.waitKey(0)
        # for i, image_path in enumerate(image_paths):
        # actual_path = os.path.join(image_folder, image_path)
        # mage = cv2.imread(actual_path)
        #eachspace = np.array(eachspace)
        #print eachspace.shape
        predict = hog_svm(eachspace, json_dict)
        HOG_result.append(predict)

    #print ("Background Thresolding: {}").format(str(BT_result))
    print ("HoG + SVM: {}").format(str(HOG_result))

    parking_location = "EEEI Student's Parking Lot"
    print ("###############################")
    print ("")
    print ("Parking Lot: {}").format(parking_location)
    print ("")

    for i, occupancy in enumerate(HOG_result):
        if HOG_result[i] == 1:
            state = "OCCUPIED"
        else:
            state = "EMPTY"
        print ("#{}: {}").format(i + 1, state)

        rectcolor = (0, 0, 255) if HOG_result[i] == 1 else (0, 255, 0)
        
        cv2.polylines(test_img, np.array([ppts[i][1]]), True, rectcolor, thickness=2)
        cv2.putText(test_img, str(i + 1), tuple(ppts[i][1][0]), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (255, 0, 0), thickness=2)
    print ("")
    print ("###############################")

    cv2.imshow("EEEI Parking Lot", test_img)
    cv2.waitKey(0)