#!/usr/bin/env python

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24 as nrf
import random

import cv2
import numpy as np

role = "controller"
inp_role = '0'

'''
node 1 only
to do: search database for found nodes
'''
nrf.config()
nrf.controller_config()

#node1
nrf.config_to_node1()

nrf.sendPacket("SEND OCCUPANCY ARRAY 1")

ack_ping = nrf.recvPacket(1)
print('Ack : {}'.format(ack_ping))
if(ack_ping == "NODE 1"):
	print('*************************************')
	print('Now accepting data from Node 1')
	received = nrf.recvTCP(1)
	while(not received == "DONE SENDING ARRAY"):
		print('Node 1: {}'.format(received))
		received = nrf.recvTCP(1)

#node2
nrf.config_to_node2()

nrf.sendPacket("SEND OCCUPANCY ARRAY 2")
ack_ping = nrf.recvPacket(2)
if(ack_ping == "NODE 2"):
	print('*************************************')
	print('Now accepting data from Node 2')
	received = nrf.recvTCP(2)
	while(not received == "DONE SENDING ARRAY"):
		print('Node 2: {}'.format(received))
		received = nrf.recvTCP(2)