#!/usr/bin/env python

from __future__ import print_function
import time
import hashlib
from RF24 import *
import RPi.GPIO as GPIO
import ast
import json
import pynrf24 as nrf
import random

import cv2
import numpy as np
import picamera
from picamera.array import PiRGBArray
from base64 import b64decode, b64encode
import zlib
from PIL import Image

role = "node"
inp_role = '2'

def waitCommand():
	while True:
		received = nrf.recvPacket(1)
		print('Received command: {}'.format(received))
		return received

'''
sends the occupancy array in string format
to do: insert HOG and BT arrays
'''
def sendOccupancyArray():
	nrf.sendPacket("NODE "+inp_role)
	occupancy_array = [1,0,1,0,0,0,1,0,1,0,0,0,1,1,1,1,1,1,1,1,1,1]
	HOG_array = [0,0,1,0,1,0,1,0,1,0,0,0,1,1,1,0,1,1,1,1,1,1]
	BT_array = [1,0,1,0,0,0,1,0,1,0,0,0,1,1,1,1,0,1,1,1,1,1]
	data_to_send = 'HOG: '+str(HOG_array)
	if(not nrf.sendTCP(data_to_send)):
		mode = nrf.chooseMode()
		runMode(mode)
	data_to_send = 'BT: '+str(BT_array)
	if(not nrf.sendTCP(data_to_send)):
		mode = nrf.chooseMode()
		runMode(mode)
	data_to_send = 'Occupancy: '+str(BT_array)
	if(not nrf.sendTCP(data_to_send)):
		mode = nrf.chooseMode()
		runMode(mode)
	nrf.sendTCP("DONE SENDING ARRAY")



def initSlots():
	nrf.sendPacket("NODE "+inp_role)

	#initialize camera
	camera = picamera.PiCamera()
	rawCapture = PiRGBArray(camera)
	time.sleep(0.1)

	#take picture
	camera.capture(rawCapture, format="bgr")
	image = rawCapture.array
	cv2.imwrite('test_image.jpg', image)
	
	#can move to exit function later
	camera.close()

	foo = Image.open('test_image.jpg')
	foo.save("compressed_opt.jpg",optimize=True,quality=75)

	with open("compressed_opt.jpg", "rb") as f:
		data = f.read()
		encoded_data = data.encode("base64")

	print('{}'.format(encoded_data))
	nrf.sendTCP(encoded_data)

if __name__ == "__main__":
	nrf.config()
	nrf.node2_config()
	nrf.waitStartNormal()

	while True:
		cmd = waitCommand()
		if(cmd == ('SEND OCCUPANCY ARRAY '+inp_role)):
			print('Waiting for Occupancy Array')
			sendOccupancyArray()
			cmd = 'WAITING'
		if(cmd == ('INITIALIZE SLOTS '+inp_role)):
			print('Waiting for Parking Lot picture')
			initSlots()
			cmd = 'WAITING'